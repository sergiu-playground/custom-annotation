package com.sergiu.playground.custom.annotation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomAnnotation {

    public static void main(String[] args) {
        SpringApplication.run(CustomAnnotation.class, args);
    }

}
