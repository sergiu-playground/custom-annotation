package com.sergiu.playground.custom.annotation.annotations.aspectj;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectJAnnotationImpl {

    @Before("@annotation(com.sergiu.playground.custom.annotation.annotations.aspectj.AspectJAnnotation)")
    public void before2() {
        System.out.println("Do stuff");
    }
}