package com.sergiu.playground.custom.annotation.controller;

import com.sergiu.playground.custom.annotation.annotations.aspectj.AspectJAnnotation;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class TestController {

    @AspectJAnnotation
    @GetMapping(value = "/test", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> test(){
        String test = "test";

        return new ResponseEntity<>(test, HttpStatus.OK);
    }

}